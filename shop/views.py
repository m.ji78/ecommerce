from django.core.paginator import Paginator
from django.shortcuts import render
from .models import ProductsModel, OrderModel


def defaultView(request):
    product_lists = ProductsModel.objects.all()

    item_name = request.GET.get('item_name')

    if item_name != '' and item_name is not None:
        product_lists = product_lists.filter(product_name__icontains=item_name)

    paginator = Paginator(product_lists, 3)
    page = request.GET.get('page')
    product_lists = paginator.get_page(page)

    context = {
        'product_lists': product_lists,
    }

    return render(request, 'mainPage.html', context)


def detailsView(request, id):
    product = ProductsModel.objects.get(pk=id)

    context = {
        'product': product,
    }

    return render(request, 'detailPage.html', context)


def checkout(request):

    if request.method == 'POST':
        items = request.POST.get('items', "")
        name = request.POST.get('name', "")
        city = request.POST.get('city', "")
        email = request.POST.get('email', "")
        state = request.POST.get('state', "")
        address = request.POST.get('address', "")
        zipcode = request.POST.get('zipcode', "")
        total_price = request.POST.get('total_price', "")

        order = OrderModel(items=items, name=name, email=email, state=state, address=address, city=city, zipcode=zipcode, total_price=total_price)
        order.save()

    return render(request, 'checkout.html')