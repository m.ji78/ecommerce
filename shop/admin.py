from django.contrib import admin
from .models import ProductsModel, OrderModel


class ProductAdmin(admin.ModelAdmin):

    def change_category_to_default(self, request, queryset):
        queryset.update(category='default')

    change_category_to_default.short_description = 'D'
    list_display = ('product_name', 'category', 'description', 'price')
    search_fields = ('product_name', 'category')
    actions = ('change_category_to_default',)
    list_editable = ('category',)
    # for product page
    # fields = ('product_name', 'price')


class OrderAdmin(admin.ModelAdmin):
    list_display = ('name', 'email', 'city', 'address', 'zipcode', 'total_price')


admin.site.register(ProductsModel, ProductAdmin)
admin.site.register(OrderModel, OrderAdmin)