from django.db import models


class ProductsModel(models.Model):

    def __str__(self):
        return self.product_name

    product_name = models.CharField(max_length=200)
    price = models.FloatField()
    discount_price = models.FloatField()
    category = models.CharField(max_length=200)
    description = models.TextField()
    image = models.CharField(max_length=1000)


class OrderModel(models.Model):

    def __str__(self):
        return self.name

    items = models.CharField(max_length=1000)
    name = models.CharField(max_length=200)
    city = models.CharField(max_length=200)
    email = models.CharField(max_length=200)
    address = models.CharField(max_length=500)
    state = models.CharField(max_length=200)
    zipcode = models.CharField(max_length=200)
    total_price = models.CharField(max_length=200)
